<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocmp4r37kofk',
  'passwordsalt' => 'vftDcgsv/MAdmXQgYDNyaZuBSujx69',
  'secret' => 'D0+YcMqos/qHoU6boZ9anWfM8lx5Xsl++HDE/wNaLWC60Csi',
  'trusted_domains' => 
  array (
    0 => '192.168.1.10:9000',
    1 => 'nextcloud_app',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'mysql',
  'version' => '23.0.3.2',
  'overwrite.cli.url' => 'http://192.168.1.10:9000',
  'dbname' => 'nextcloud',
  'dbhost' => 'nextcloud_db',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'nextcloud_password',
  'installed' => true,
);
