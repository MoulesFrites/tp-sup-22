# tp-sup-22

## Partie Nagios

### Installation

```bash
sed -i 's/SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
setenforce 0
yum install -y gcc glibc glibc-common wget unzip httpd php gd gd-devel perl postfix make gettext automake autoconf openssl-devel net-snmp net-snmp-utils epel-release
cd /tmp
wget -O nagioscore.tar.gz https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.6.tar.gz
tar xzf nagioscore.tar.gz
cd /tmp/nagioscore-nagios-4.4.6/
./configure
make all
make install-groups-users
usermod -aG nagios apache
make install
make install-daemoninit
systemctl enable httpd.service
make install-commandmode
make install-config
make install-webconf
firewall-cmd --zone=public --add-port=80/tcp --permanent
htpasswd -c /usr/local/nagios/etc/htpasswd.users nagiosadmin
systemctl start httpd.service
systemctl enable httpd.service
systemctl start nagios.service
systemctl enable nagios.service
yum install -y perl-Net-SNMP
cd /tmp
wget https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ihv epel-release-latest-7.noarch.rpm
wget --no-check-certificate -O nagios-plugins.tar.gz https://github.com/nagios-plugins/nagios-plugins/archive/release-2.3.3.tar.gz
tar zxf nagios-plugins.tar.gz
cd /tmp/nagios-plugins-release-2.3.3/
./tools/setup
./configure
make install
systemctl restart nagios.service
```

### Alias

```bash
echo "alias testNagios='/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg'" >> /root/.bashrc
echo "alias nagios='/usr/local/nagios/bin/nagios /usr/local/nagios/bin/nagios'" >> /root/.bashrc
```

### Checks

```bash
./check_ping -H localhost -w 20,2% -c 20,30%
./check_ping -H www.duckduckgo.fr -w 10,10% -c 20,20%
./check_http -H localhost -u http://localhost/nagios -a nagiosadmin:nagiosadmin
```

### Creation host nagios

```yaml
vim /usr/local/nagios/etc/objects/serveur_nagios.cfg

    define command {

      command_name check-ssh-localhost
      command_line $USER1$/check_ssh localhost
    }

    define host {
      host_name nagios
      address localhost
      check_command check-ping-localhost
      max_check_attempts 3
    }

    define service {

      service_description SSH on nagios
      host_name nagios
      check_command check-ssh-localhost
      max_check_attempts 3
    }
```

#### Nagios est fonctionnel et a 2 hosts et 9 checks
